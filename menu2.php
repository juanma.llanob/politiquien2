<!-- Top Header Area -->
<div class="top-header-area">
    <div class="container">

        <div class="col-12">
            <div class="top-header-content  ">
                <!-- Logo -->
                <div class="logo ">
                    <a class=" text-center" href="index.php"><img class=" my-4" src="img/politquien_logo.png" alt=""></a>
                </div>

                
            </div>
        </div>
    </div>

</div>


<!-- Navbar Area -->
<div class="newspaper-main-menu" id="stickyMenu">
    <div class="classy-nav-container breakpoint-off">
        <div class="container">
            <!-- Menu -->
            <nav class="classy-navbar justify-content-between" id="newspaperNav">

                <!-- Logo -->
                <div class="logo">
                    <a href="index.php"><img src="img/poliquien-blanco.png" alt=""></a>
                </div>

                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <!-- Menu -->
                <div class="classy-menu">

                    <!-- close btn -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>

                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul>
                            <li class="text-menu"><a href="index.php">Inicio</a></li>
                            
                            <li class="text-menu"><a href="index2.php" target="_blank">Primera vuelta</a>
                            </li>
                            <li class="text-menu"><a href="temas.php">temas</a></li>
                            <li class="text-menu"><a href="nosotros.php">Nosotros</a></li>
                            <li class="text-menu"><a href="auspiciadores.php">Auspiciadores</a></li>
                            <!-- <li class="text-menu"><a href=".php">Compartir</a></li> -->
                        </ul>
                    </div>
                    <!-- Nav End -->
                </div>
            </nav>
        </div>
    </div>
</div>
