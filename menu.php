
<!-- Navigation -->
<script>

    $(document).on('click', '.reCan', function () {
// code here
        var targetId = event.target.id;
        $.redirect('Partido.php', {'id': targetId});

        console.log(targetId);
    });
    $(document).on('click', '.irProvincia2', function () {
// code here
        var targetId = event.target.id;
        $.redirect('Provincia.php', {'idProvincia': targetId,'enfocar':1});

        console.log(targetId);
    });
    $(document).on('click', '.irDistrito2', function () {
// code here
        var targetId = event.target.id;
        $.redirect('Distrito.php', {'idDistrito': targetId,'enfocar':1});

        console.log(targetId);
    });


</script>
<style type="text/css">
    body{
        font-family: Myriad pro;
    }
    .text-logo{
        font-size: 32px;
    }
    .text-menu{
        font-size: 20px;
    }

    .text-bold{
      font-weight: bold;
    }
    .text-tabla{
        font-size: 18px;
    }
    .text-titulo{
      font-size: 40px;
    }
    .text-subtitulo{
      font-size: 26px;
    }
    .text-contenido{
      font-size: 18px;

    }    
    .text-ai{
        font-size: 36px;
        text-shadow: 0px 0px 10px black;
    }
    .color-rojo{
        color:#a62b30;

    }
    .color-gris{
        color: #464646;
    }



</style>
<div class="container">
    <div class="row ">
        <div class ="col-lg-2 col-sm-2 my-auto p-0">
            <a href="./index.php" class="w-100">
                <img class="w-100"  src="imagenes/fwlogopolitiquien/mainLogo.jpeg" alt="Chania">
            </a>
        </div>

        <div class="col-lg-8 col-sm-8 h-100">
            
            <div class="row ">
                <div class=" w-100 text-center">
                    <p class=" text-logo">Elecciones Regionales y Municipales 2018 Arequipa</p>
                    <nav class="navbar navbar-expand-lg navbar-expand-sm navbar-light w-100 ">

                        <div class="collapse navbar-collapse  w-100" id="navbarNavDropdown">

                            <ul class="nav navbar-nav navbar-expand-sm w-100 ">
                                <div class="col-lg-3 col-sm-3 ">
                                    <li class="nav-item dropdown misBordes  ">
                                        <a class="nav-link dropdown-toggle  text-center" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Candidatos
                                        </a>
                                        <div class="dropdown-menu " aria-labelledby="navbarDropdownMenuLink">
                                            <a  id=0 class = " dropdown-item irDistrito2"> Distritos</a>
                                            <a id=0 class="dropdown-item irProvincia2" >Provincias</a>
                                            <a class="dropdown-item" href="./">Región</a>
                                        </div>
                                    </li>

                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <li class="nav-item dropdown  misBordes ">
                                        <a class="nav-link dropdown-toggle text-center" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Agrupación Política
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <?php
                                            require __DIR__ . '/modelo/Modelo.php';

                                            require __DIR__ . '/modelo/ModeloPartido.php';
                                            $model = ModeloPartido::getInstance();
                                            $we = $model->getLista();
                                            $array = json_decode($we, true);
                                            foreach ($array as $valor) {
                                                echo "<a class=\"dropdown-item  reCan\" id=\"$valor[IdPartidoPolitico]\" href=\"#\">$valor[PartidoPoliticoNombre]</a>";
                                            }
                                            ?>
                                        </div>
                                    </li>
                                </div>

                                <div class="col-lg-3 col-sm-3">
                                    <li class="nav-item dropdown misBordes  " >
                                        <a class="nav-link dropdown-toggle text-center" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Provincia
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <a id="1" class="dropdown-item irProvincia2" >Arequipa</a>
                                            <a id="8" class="dropdown-item irProvincia2" >Islay</a>
                                           </div>
                                    </li>


                                </div>
                            </ul>
                        </div>
                    </nav>
                </div>


            </div>

        </div>
        <div class ="col-lg-2 col-sm-2 my-auto p-0  ">
            <a target="_blank" href="https://www.meetup.com/es/Hacks-Hackers-Arequipa/?_cookie-check=2NSkxgFJHN2Mh3cZ" class="w-100">
                <img class=" w-100" src="imagenes/fwlogopolitiquien/hack.PNG" alt="Chania">
            </a>
        </div>
    </div>

</div>
<hr class="style13">
