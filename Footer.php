
<!-- Footer -->
<style type="text/css">
img{
  width: 75%;
}
.tabla{
  display: table;
}
.row1{
  display: table-row;
}
.cell{
  display: table-cell;
}
</style>
<footer class="mt-5 py-4 bg-dark">
  <div class="container p-0">
    <div class="row">
      <div class="col-lg-6">
        <p class="text-white text-contenido ">Copyright &copy; PolitiQuien 2018 </p>
      </div>
      <div class="col-lg-6">
        <p class="text-contenido text-white text-bold">Auspiciadores:</p>
        <div class="tabla text-center">
          <div class=" row1">
            <a href="http://www.unsa.edu.pe" target="_blank" class="btn cell"><img class="rounded " src="imagenes/todoslosiconosylogos/unsa.png" alt="Chania"></a>
            <a href="https://www.facebook.com/jakuemprendeunsa/" target="_blank" class="btn cell"><img class="rounded " src="imagenes/todoslosiconosylogos/jaku.png" alt="Chania"></a>
            <a href="https://www.facebook.com/ideaenperu" target="_blank" class="btn cell"><img class="rounded " src="imagenes/todoslosiconosylogos/idea.png" alt="Chania"></a>
            <a href="http://www.politiquem.com " target="_blank" class="btn cell"><img class="rounded " src="imagenes/todoslosiconosylogos/politiquien.png" alt="Chania"></a>
            <a href="https://elbuho.pe" target="_blank" class="btn "><img class="rounded " src="imagenes/todoslosiconosylogos/elbuho.png" alt="Chania"></a>
            <a href="https://larepublica.pe" target="_blank" class="btn cell"><img class="rounded " src="imagenes/todoslosiconosylogos/republica.png" alt="Chania"></a>
            <a href="https://rpp.pe" target="_blank" class="btn cell"><img class="rounded " src="imagenes/todoslosiconosylogos/rpp.png" alt="Chania"></a>
            <a href="http://radioyaravi.org.pe" target="_blank" class="btn cell"><img class="rounded " src="imagenes/todoslosiconosylogos/yaravi.png" alt="Chania"></a>
            <a href="http://mollendinos.com/" target="_blank" class="btn cell"><img class="rounded " src="imagenes/todoslosiconosylogos/star logo_pn1.jpg" alt="Chania"></a>
            <a href="http://mollendinos.com/" target="_blank" class="btn cell"><img class="rounded " src="imagenes/todoslosiconosylogos/logo_mollen1.jpg" alt="Chania"></a>
            <a href="https://chicaspoderosas.org/2015/11/20/chicas-poderosas-peru-2/" target="_blank" class="btn cell"><img class="rounded " src="imagenes/todoslosiconosylogos/logoChicasPod.png" alt="Chania"></a>
          </div>

        </div>

      </div>

    </div>
  </div>
  <!-- /.container -->
</footer>