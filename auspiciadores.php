<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Politiquien</title>


  <link rel="icon" href="img/HK.ico">

  <link rel="stylesheet" href="style.css">
</head>
<body>

 <header class="header-area">

  <?php
  include 'menu2.php';
  ?>
</header>



<section class="about-area w-100 text-center" >
 <div class="container col-12 col-md-10 col-lg-8 ">

   <div class="row text-left">
     <div class="col-12 mt-5">
       <h2>Socios</h2>
     </div>
   </div>

   <div class="row text-left">

     <div class="col-12" >
      <p align="justify">Es una plataforma colaborativa que proporciona información sobre los perfiles de los candidatos en las Elecciones Regionales y Municipales 2018 en Arequipa. Concretamente, los aspirantes al gobierno regional, los municipios provinciales y distritales de las provincias de Arequipa e Islay.</p>
      <p align="justify">Tomamos el nombre del aplicativo Politiquem que desarrollaron colegas en Brasil y nos han servido de inspiración. Este proyecto se desarrolla bajo el auspicio de la organización Chicas Poderosas.</p>
      <p align="justify">Para la segunda vuelta, desarrollamos un aplicativo que muestra las posiciones de los dos candidatos al gobierno regional que participan en esta etapa. Con esta información, haremos un ejercicio de fact checking durante la fase final de la campaña, en las presentaciones públicas de ambos candidatos.</p>

      <p align="justify">PolitiQuem es una plataforma que apuesta en el periodismo colaborativo y trabaja en asociación con vehículos destacados. Juntos mapeamos una inmensa cantidad de información sobre las candidatas y los candidatos a las elecciones para verificar y presentar los contenidos más relevantes de forma organizada para los electores. </p>
      <p><strong>Si desea unirse a la plataforma de PolitiQuem, mande su propuesta en el e-mail: politiquem.brasil@gmail.com</strong></p>

    </div>
  </div>
</div>
</section>

<section class="newspaper-team my-3 w-100 text-cente">
  <div class="container col-12 col-md-10 col-lg-8">

    <div class="row">

      <!-- Single Team Member -->
      <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="single-team-member text-center my-3 p-4">
          <a href="http://www.unsa.edu.pe" target="_blank" ><img src="imagenes/todoslosiconosylogos/unsa.png" alt="">
            <div class="team-info mt-4">
              <h6>Universidad Nacional de San Agustín</h6>
            </div>
          </a>
        </div>
      </div>

      <!-- Single Team Member -->
      <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="single-team-member text-center my-3 p-4">
          <a href="https://www.facebook.com/jakuemprendeunsa/" target="_blank">
            <img src="imagenes/todoslosiconosylogos/jaku.png" alt="">
            <div class="team-info mt-4">
              <h6>JAKU Emprende UNSA</h6>
            </div>
          </a>
        </div>
      </div>

      <!-- Single Team Member -->
      <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="single-team-member text-center my-3 p-4">
          <a href="https://www.facebook.com/ideaenperu" target="_blank">
            <img src="imagenes/todoslosiconosylogos/idea.png" alt="">
            <div class="team-info mt-4">
              <h6>Idea Internacional Perú</h6>
            </div>
          </a>
        </div>
      </div>

      <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="single-team-member text-center my-3 p-4">
          <a href="http://www.politiquem.com " target="_blank">
            <img src="imagenes/todoslosiconosylogos/politiquien.png" alt="">
            <div class="team-info mt-4">
              <h6>Politiquien Brasil</h6>
            </div>
          </a>
        </div>
      </div>

      <!-- Single Team Member -->
      <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="single-team-member text-center my-3 p-4">
          <a href="https://elbuho.pe" target="_blank">
            <img src="imagenes/todoslosiconosylogos/elbuho.png" alt="">
            <div class="team-info mt-4">
              <h6>El Búho</h6>
            </div>
          </a>
        </div>
      </div>

      <!-- Single Team Member -->
      <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="single-team-member text-center my-3 p-4">
          <a href="https://larepublica.pe" target="_blank">
            <img src="imagenes/todoslosiconosylogos/republica.png" alt="">
            <div class="team-info mt-4">
              <h6>La República</h6>
            </div>
          </a>
        </div>
      </div>

      <!-- Single Team Member -->
      <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="single-team-member text-center my-3 p-4">
          <a href="https://rpp.pe" target="_blank">
            <img src="imagenes/todoslosiconosylogos/rpp.png" alt="">
            <div class="team-info mt-4">
              <h6>RPP</h6>
            </div>
          </a>
        </div>
      </div>

      <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="single-team-member text-center my-3 p-4">
          <a href="http://radioyaravi.org.pe" target="_blank">
            <img src="imagenes/todoslosiconosylogos/yaravi.png" alt="">
            <div class="team-info mt-4">
              <h6>Radio Yaravi</h6>
            </div>
          </a>
        </div>
      </div>

      <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="single-team-member text-center my-3 p-4">
          <a href="http://mollendinos.com/" target="_blank">
            <img src="imagenes/todoslosiconosylogos/star logo_pn1.jpg" alt="">
            <div class="team-info mt-4">
              <h6>Radio Star Mollendo</h6>
            </div>
          </a>
        </div>
      </div>

      <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="single-team-member text-center my-3 p-4">
          <a href="http://mollendinos.com/" target="_blank">
            <img src="imagenes/todoslosiconosylogos/logo_mollen1.jpg" alt="">
            <div class="team-info mt-4">
              <h6>Mollendinos.com</h6>
            </div>
          </a>
        </div>
      </div>

      <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="single-team-member text-center my-3 p-4">
          <a href="https://chicaspoderosas.org/2015/11/20/chicas-poderosas-peru-2/" target="_blank">
            <img src="imagenes/todoslosiconosylogos/logoChicasPod.png" alt="">
            <div class="team-info mt-4">
              <h6>Chicas Poderosas Global</h6>
            </div>
          </a>
        </div>
      </div>

    </div>
  </div>
</section>


<div class="footer w-100 mt-5">
  <?php
  include 'FooterP.php';
  ?>
</div>

<script src="js/jquery/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="js/bootstrap/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="js/bootstrap/bootstrap.min.js"></script>
<!-- All Plugins js -->
<script src="js/plugins/plugins.js"></script>
<!-- Active js -->
<script src="js/active.js"></script>

</body>
</html>